.PHONY: clean real-clean

all: devis.pdf facture.pdf

devis.pdf: devis.tex client.tex company_defs.tex devis_defs.tex table.tex
	pdflatex devis.tex
	pdflatex devis.tex

facture.pdf: devis.tex client.tex company_defs.tex devis_defs.tex table.tex facture_defs.tex
	pdflatex facture.tex
	pdflatex facture.tex

clean:
	rm -f *.aux *.log

real-clean:
	rm -f *.aux *.log *.pdf
