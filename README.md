# Devis et facture en LaTeX

# Configuration

Éditez les fichiers suivants pour configurer votre devis ou votre facture :
- company\_defs.tex : définition des coordonnées de votre entreprise
- client.tex : définition des coordonnées de votre client
- devis\_defs.tex : définition des informations relatives au devis
- facture\_defs.tex : définition du n° de la facture
- table.tex : la table des prestations (attention à modifier le `-2` de `\textbf{:={sum([0,-2]:[0,-1])}\,€}` si vous ajoutez ou retirez des lignes)
- annexes.tex : facultatif, vous pouvez ajoutez des annexes à vos devis ou factures ou supprimer ce fichier pour ne pas ajouter d’annexe.

## Compilation

Vous aurez besoin de `pdflatex` et sans doute de plein de paquets LaTeX.

```
pdflatex devis.tex
pdflatex facture.tex
```

NB : Vous aurez besoin de compiler les documents deux fois si vous faites des références (comme dans l’exemple donné).

Si vous avez `make` :
```
make devis.pdf
make facture.pdf
# Ou pour faire les deux en même temps
make
```
